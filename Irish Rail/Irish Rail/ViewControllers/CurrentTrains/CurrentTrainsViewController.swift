//
//  CurrentTrainsViewController.swift
//  Irish Rail
//
//  Created by Khatib Designs on 2/5/21.
//

import UIKit

class CurrentTrainsViewController: UIViewController {
    
    // MARK: Variables
    private var trains = [Trains]()
    private var reuseId = "TrainsTableViewCell"
    
    // MARK: Outlets
    @IBOutlet weak var allButton: UIButton!
    @IBOutlet weak var mainlineButton: UIButton!
    @IBOutlet weak var suburbanButton: UIButton!
    @IBOutlet weak var dartButton: UIButton!
    @IBOutlet weak var trainsTableView: UITableView!
    
    // MARK: Actions
    @IBAction func allAction(_ sender: Any) {
        getTrainsByType(type: "A")
    }
    @IBAction func mainlineAction(_ sender: Any) {
        getTrainsByType(type: "M")
    }
    @IBAction func suburbanAction(_ sender: Any) {
        getTrainsByType(type: "S")
    }
    @IBAction func dartAction(_ sender: Any) {
        getTrainsByType(type: "D")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupTableView()
        getCurrentTrains()
    }
    
    private func getCurrentTrains() {
        API().getCurrentTrainsXML { (data) in
            self.trains = data
            self.trainsTableView.reloadData()
        } failure: {
            self.trains.removeAll()
            self.trainsTableView.reloadData()
        }
    }
    private func getTrainsByType(type: String) {
        API().getCurrentTrainsXML(byType: "\(type)", completion: { (data) in
            self.trains = data
            self.trainsTableView.reloadData()
        }, failure: {
            self.trains.removeAll()
            self.trainsTableView.reloadData()
        })
    }
    private func setupTableView() {
        self.trainsTableView.register(UINib(nibName: "\(reuseId)", bundle: nil), forCellReuseIdentifier: "\(reuseId)")
        self.trainsTableView.delegate = self
        self.trainsTableView.dataSource = self
    }
}
extension CurrentTrainsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.trains.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "\(reuseId)", for: indexPath) as! TrainsTableViewCell
                
        cell.configureCell(status: "\(self.trains[indexPath.row].TrainStatus ?? "")",
                           lat: "Lat \(self.trains[indexPath.row].TrainLatitude ?? 0.0)",
                           long: "Long \(self.trains[indexPath.row].TrainLongitude ?? 0.0)",
                           code: "\(self.trains[indexPath.row].TrainCode ?? "")",
                           date: "\(self.trains[indexPath.row].TrainDate ?? "")",
                           message: "\(self.trains[indexPath.row].PublicMessage ?? "")",
                           direction: "\(self.trains[indexPath.row].Direction ?? "")")
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

