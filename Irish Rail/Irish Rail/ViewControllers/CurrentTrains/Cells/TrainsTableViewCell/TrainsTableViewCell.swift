//
//  TrainsTableViewCell.swift
//  Irish Rail
//
//  Created by Khatib Designs on 2/5/21.
//

import UIKit

class TrainsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var trainStatusLabel: UILabel!
    @IBOutlet weak var trainLatLabel: UILabel!
    @IBOutlet weak var trainLongLabel: UILabel!
    @IBOutlet weak var trainCodeLabel: UILabel!
    @IBOutlet weak var trainDateLabel: UILabel!
    @IBOutlet weak var publicMessageLabel: UILabel!
    @IBOutlet weak var trainDirectionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell(status: String, lat: String, long: String, code: String, date: String, message: String, direction: String) {
        self.trainCodeLabel.layer.cornerRadius = 4
        self.trainCodeLabel.clipsToBounds = true

        self.trainStatusLabel.text! = "\(status)"
        self.trainLatLabel.text! = "\(lat)"
        self.trainLongLabel.text! = "\(long)"
        self.trainCodeLabel.text! = "\(code)"
        self.trainDateLabel.text! = "\(date)"
        self.publicMessageLabel.text! = "\(message)"
        self.trainDirectionLabel.text! = "\(direction)"
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
