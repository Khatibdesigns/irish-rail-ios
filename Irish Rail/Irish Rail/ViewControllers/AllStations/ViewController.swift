//
//  ViewController.swift
//  Irish Rail
//
//  Created by Khatib Designs on 2/4/21.
//

import UIKit

class ViewController: UIViewController {

    // MARK: Variables
    private var stations = [Stations]()
    private var reuseId = "StationsTableViewCell"
    
    // MARK: Outlets
    @IBOutlet weak var allButton: UIButton!
    @IBOutlet weak var mainlineButton: UIButton!
    @IBOutlet weak var suburbanButton: UIButton!
    @IBOutlet weak var dartButton: UIButton!
    @IBOutlet weak var stationsTableView: UITableView!
    
    // MARK: Actions
    @IBAction func allAction(_ sender: Any) {
        getAllStationsByType(type: "A")
    }
    @IBAction func mainlineAction(_ sender: Any) {
        getAllStationsByType(type: "M")
    }
    @IBAction func suburbanAction(_ sender: Any) {
        getAllStationsByType(type: "S")
    }
    @IBAction func dartAction(_ sender: Any) {
        getAllStationsByType(type: "D")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupTableView()
        getAllStations()
    }
    
    private func getAllStations() {
        API().getAllStationsXML { (data) in
            self.stations = data
            self.stationsTableView.reloadData()
        } failure: {
            self.stations.removeAll()
            self.stationsTableView.reloadData()
        }
    }
    private func getAllStationsByType(type: String) {
        API().getAllStationsXML(byType: "\(type)", completion: { (data) in
            self.stations = data
            self.stationsTableView.reloadData()
        }, failure: {
            self.stations.removeAll()
            self.stationsTableView.reloadData()
        })
    }
    private func setupTableView() {
        self.stationsTableView.register(UINib(nibName: "\(reuseId)", bundle: nil), forCellReuseIdentifier: "\(reuseId)")
        self.stationsTableView.delegate = self
        self.stationsTableView.dataSource = self
    }
    
    @objc private func stationData(sender: UIButton) {
        let index = sender.tag
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "StationDataViewController") as! StationDataViewController
        VC.stationCode = "\(self.stations[index].StationCode ?? "")"
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "stationDataSegue" {
            let VC = segue.destination as! StationDataViewController
            VC.stationDesc = "\(sender ?? "")"
        }
    }
}
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.stations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "\(reuseId)", for: indexPath) as! StationsTableViewCell
        
        cell.configureCell(description: "\(self.stations[indexPath.row].StationDesc ?? "")",
                           code: "\(self.stations[indexPath.row].StationCode ?? "")",
                           id: "#\(self.stations[indexPath.row].StationId ?? 0)",
                           lat: "Lat \(self.stations[indexPath.row].StationLatitude ?? 0.0)",
                           long: "Long \(self.stations[indexPath.row].StationLongitude ?? 0.0)")
        
        cell.stationCodeButton.tag = indexPath.row
        cell.stationCodeButton.addTarget(self, action: #selector(self.stationData(sender:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "stationDataSegue", sender: "\(self.stations[indexPath.row].StationDesc ?? "")")
    }
}

