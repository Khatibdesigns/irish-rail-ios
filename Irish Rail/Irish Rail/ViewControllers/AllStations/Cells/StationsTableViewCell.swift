//
//  StationsTableViewCell.swift
//  Irish Rail
//
//  Created by Khatib Designs on 2/5/21.
//

import UIKit

class StationsTableViewCell: UITableViewCell {

    @IBOutlet weak var stationDescLabel: UILabel!
    @IBOutlet weak var stationCodeButton: UIButton!
    @IBOutlet weak var stationIdLabel: UILabel!
    @IBOutlet weak var stationLatLabel: UILabel!
    @IBOutlet weak var stationLongLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell(description: String, code: String, id: String, lat: String, long: String) {
        self.stationCodeButton.layer.cornerRadius = 4
        self.stationCodeButton.clipsToBounds = true
        self.stationDescLabel.text! = "\(description)"
        self.stationCodeButton.setTitle("\(code)", for: .normal)
        self.stationIdLabel.text! = "\(id)"
        self.stationLatLabel.text! = "\(lat)"
        self.stationLongLabel.text! = "\(long)"
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
