//
//  TrainMovementsViewController.swift
//  Irish Rail
//
//  Created by Khatib Designs on 2/8/21.
//

import UIKit

class TrainMovementsViewController: UIViewController {

    // MARK: Variables
    private var reuseId = "TrainMovementsTableViewCell"
    var trainCode: String?
    var trainDate: String?
    private var movementData = [TrainMovements]()
    
    // MARK: Outlets
    @IBOutlet weak var trainMovementsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupTableView()
        getMovementData()
    }
    
    private func setupTableView() {
        self.trainMovementsTableView.register(UINib(nibName: "\(reuseId)", bundle: nil), forCellReuseIdentifier: "\(reuseId)")
        self.trainMovementsTableView.delegate = self
        self.trainMovementsTableView.dataSource = self
    }
    private func getMovementData() {
        API().getTrainMovements(TrainId: "\(trainCode ?? "")", TrainDate: "\(trainDate ?? "")") { (data) in
            self.movementData = data
            self.trainMovementsTableView.reloadData()
        } failure: {
            self.movementData.removeAll()
            self.trainMovementsTableView.reloadData()
        }
    }
}
extension TrainMovementsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.movementData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "\(reuseId)", for: indexPath) as! TrainMovementsTableViewCell
                
        cell.configureCell(origin: "\(self.movementData[indexPath.row].TrainOrigin ?? "")",
                           code: "\(self.trainCode ?? "")",
                           destination: "\(self.movementData[indexPath.row].TrainDestination ?? "")",
                           date: "\(self.trainDate ?? "")",
                           departure: "\(self.movementData[indexPath.row].ScheduledDeparture ?? "")",
                           arrival: "\(self.movementData[indexPath.row].ExpectedArrival  ?? "")")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
