//
//  TrainMovementsTableViewCell.swift
//  Irish Rail
//
//  Created by Khatib Designs on 2/8/21.
//

import UIKit

class TrainMovementsTableViewCell: UITableViewCell {

    @IBOutlet weak var originLabel: UILabel!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var destinationLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var departureLabel: UILabel!
    @IBOutlet weak var arrivalLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell(origin: String, code: String, destination: String, date: String, departure: String, arrival: String) {
        self.codeLabel.layer.cornerRadius = 4
        self.codeLabel.clipsToBounds = true

        self.originLabel.text! = "\(origin)"
        self.codeLabel.text! = "\(code)"
        self.destinationLabel.text! = "\(destination)"
        self.dateLabel.text! = "\(date)"
        self.departureLabel.text! = "Scheduled Departure: \(departure)"
        self.arrivalLabel.text! = "Expected Arrival: \(arrival)"
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        
    }
}
