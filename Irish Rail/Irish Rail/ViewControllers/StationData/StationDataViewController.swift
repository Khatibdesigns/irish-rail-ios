//
//  StationDataViewController.swift
//  Irish Rail
//
//  Created by Khatib Designs on 2/5/21.
//

import UIKit

class StationDataViewController: UIViewController {

    // MARK: Variables
    private var reuseId = "TrainsTableViewCell"
    var stationDesc: String?
    var stationCode: String?
    private var stationData = [StationData]()
    
    // MARK: Outlets
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var timeSlider: UISlider!
    @IBOutlet weak var stationDataTableView: UITableView!
    
    // MARK: Actions
    @IBAction func timeSliderAction(_ sender: UISlider) {
        self.timeLabel.text! = "Time \(Int(sender.value))"
        if stationDesc != nil {
            getStationDataByTime(time: "\(Int(sender.value))")
        }else {
            getStationDataByCodeTime(time: "\(Int(sender.value))")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.timeLabel.text! = "Time \(Int(timeSlider.value))"
        
        setupTableView()
        if stationDesc != nil {
            getStationData()
        }else {
            getStationDataByCode()
        }
    }
    
    private func getStationData() {
        API().getStationData(byName: "\(stationDesc ?? "")") { (data) in
            self.stationData = data
            self.stationDataTableView.reloadData()
        } failure: {
            self.stationData.removeAll()
            self.stationDataTableView.reloadData()
        }
    }
    private func getStationDataByTime(time: String) {
        API().getStationData(byName: "\(stationDesc ?? "")", time: "\(time)") { (data) in
            self.stationData = data
            self.stationDataTableView.reloadData()
        } failure: {
            self.stationData.removeAll()
            self.stationDataTableView.reloadData()
        }
    }
    private func getStationDataByCode() {
        API().getStationData(byCode: "\(stationCode ?? "")") { (data) in
            self.stationData = data
            self.stationDataTableView.reloadData()
        } failure: {
            self.stationData.removeAll()
            self.stationDataTableView.reloadData()
        }
    }
    private func getStationDataByCodeTime(time: String) {
        API().getStationData(byCode: "\(stationCode ?? "")", time: "\(time)") { (data) in
            self.stationData = data
            self.stationDataTableView.reloadData()
        } failure: {
            self.stationData.removeAll()
            self.stationDataTableView.reloadData()
        }
    }
    private func setupTableView() {
        self.stationDataTableView.register(UINib(nibName: "\(reuseId)", bundle: nil), forCellReuseIdentifier: "\(reuseId)")
        self.stationDataTableView.delegate = self
        self.stationDataTableView.dataSource = self
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "trainMovementsSegue" {
            let VC = segue.destination as! TrainMovementsViewController
            let indexPath = sender as! IndexPath
            let index = indexPath.row
            
            VC.trainCode = "\(self.stationData[index].Traincode ?? "")"
            VC.trainDate = "\(self.stationData[index].Traindate ?? "")"
        }
    }
}
extension StationDataViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.stationData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "\(reuseId)", for: indexPath) as! TrainsTableViewCell
                
        cell.configureCell(status: "\(self.stationData[indexPath.row].Status ?? "")",
                           lat: "",
                           long: "",
                           code: "\(self.stationData[indexPath.row].Traincode ?? "")",
                           date: "\(self.stationData[indexPath.row].Traindate ?? "")",
                           message: "",
                           direction: "\(self.stationData[indexPath.row].Direction ?? "")")
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "trainMovementsSegue", sender: indexPath)
    }
}
