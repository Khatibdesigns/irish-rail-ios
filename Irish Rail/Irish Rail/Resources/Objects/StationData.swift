//
//  StationData.swift
//  Irish Rail
//
//  Created by Khatib Designs on 2/5/21.
//

import Foundation
import SWXMLHash

struct StationData: XMLIndexerDeserializable {
    
    var Servertime: String?
    var Traincode: String?
    var Stationfullname: String?
    var Stationcode: String?
    var Querytime: String?
    var Traindate: String?
    var Origin: String?
    var Destination: String?
    var Origintime: String?
    var Destinationtime: String?
    var Status: String?
    var Lastlocation: String?
    var Duein: String?
    var Late: String?
    var Exparrival: String?
    var Expdepart: String?
    var Scharrival: String?
    var Schdepart: String?
    var Direction: String?
    var Traintype: String?
    var Locationtype: String?
    
    static func deserialize(_ node: XMLIndexer) throws -> StationData {
        return try StationData(Servertime: node["Servertime"].value(),
                               Traincode: node["Traincode"].value(),
                               Stationfullname: node["Stationfullname"].value(),
                               Stationcode: node["Stationcode"].value(),
                               Querytime: node["Querytime"].value(),
                               Traindate: node["Traindate"].value(),
                               Origin: node["Origin"].value(),
                               Destination: node["Destination"].value(),
                               Origintime: node["Origintime"].value(),
                               Destinationtime: node["Destinationtime"].value(),
                               Status: node["Status"].value(),
                               Lastlocation: node["Lastlocation"].value(),
                               Duein: node["Duein"].value(),
                               Late: node["Late"].value(),
                               Exparrival: node["Exparrival"].value(),
                               Expdepart: node["Expdepart"].value(),
                               Scharrival: node["Scharrival"].value(),
                               Schdepart: node["Schdepart"].value(),
                               Direction: node["Direction"].value(),
                               Traintype: node["Traintype"].value(),
                               Locationtype: node["Locationtype"].value())
        }
}
