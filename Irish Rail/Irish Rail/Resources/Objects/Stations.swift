//
//  Stations.swift
//  Irish Rail
//
//  Created by Khatib Designs on 2/4/21.
//

import Foundation
import SWXMLHash

struct Stations: XMLIndexerDeserializable {
    
    var StationDesc: String?
    var StationLatitude: Float?
    var StationLongitude: Float?
    var StationCode: String?
    var StationId: Int?
    
    static func deserialize(_ node: XMLIndexer) throws -> Stations {
        return try Stations(StationDesc: node["StationDesc"].value(),
                            StationLatitude: node["StationLatitude"].value(),
                            StationLongitude: node["StationLongitude"].value(),
                            StationCode: node["StationCode"].value(),
                            StationId: node["StationId"].value())
        }
}
