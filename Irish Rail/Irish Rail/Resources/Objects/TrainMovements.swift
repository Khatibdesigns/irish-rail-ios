//
//  TrainMovements.swift
//  Irish Rail
//
//  Created by Khatib Designs on 2/8/21.
//

import Foundation
import SWXMLHash

struct TrainMovements: XMLIndexerDeserializable {

    var Traincode: String?
    var Traindate: String?
    var LocationCode: String?
    var LocationFullName: String?
    var LocationOrder: String?
    var Locationtype: String?
    var TrainOrigin: String?
    var TrainDestination: String?
    var ScheduledArrival: String?
    var ScheduledDeparture: String?
    var ExpectedArrival: String?
    var ExpectedDeparture: String?
    var Arrival: String?
    var Departure: String?
    var AutoArrival: String?
    var AutoDepart: String?
    var StopType: String?
    
    static func deserialize(_ node: XMLIndexer) throws -> TrainMovements {
        return try TrainMovements(Traincode: node["Traincode"].value(),
                                  Traindate: node["Traindate"].value(),
                                  LocationCode: node["LocationCode"].value(),
                                  LocationFullName: node["LocationFullName"].value(),
                                  LocationOrder: node["LocationOrder"].value(),
                                  Locationtype: node["Locationtype"].value(),
                                  TrainOrigin: node["TrainOrigin"].value(),
                                  TrainDestination: node["TrainDestination"].value(),
                                  ScheduledArrival: node["ScheduledArrival"].value(),
                                  ScheduledDeparture: node["ScheduledDeparture"].value(),
                                  ExpectedArrival: node["ExpectedArrival"].value(),
                                  ExpectedDeparture: node["ExpectedDeparture"].value(),
                                  Arrival: node["Arrival"].value(),
                                  Departure: node["Departure"].value(),
                                  AutoArrival: node["AutoArrival"].value(),
                                  AutoDepart: node["AutoDepart"].value(),
                                  StopType: node["StopType"].value())
        }
}
