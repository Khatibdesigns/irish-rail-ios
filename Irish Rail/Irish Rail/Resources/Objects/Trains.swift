//
//  Trains.swift
//  Irish Rail
//
//  Created by Khatib Designs on 2/5/21.
//

import Foundation
import SWXMLHash

struct Trains: XMLIndexerDeserializable {
    
    var TrainStatus: String?
    var TrainLatitude: Float?
    var TrainLongitude: Float?
    var TrainCode: String?
    var TrainDate: String?
    var PublicMessage: String?
    var Direction: String?
    
    static func deserialize(_ node: XMLIndexer) throws -> Trains {
        return try Trains(TrainStatus: node["TrainStatus"].value(),
                          TrainLatitude: node["TrainLatitude"].value(),
                          TrainLongitude: node["TrainLongitude"].value(),
                          TrainCode: node["TrainCode"].value(),
                          TrainDate: node["TrainDate"].value(),
                          PublicMessage: node["PublicMessage"].value(),
                          Direction: node["Direction"].value())
        }
}
