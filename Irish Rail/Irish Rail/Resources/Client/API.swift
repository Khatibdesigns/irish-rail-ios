//
//  API.swift
//  Irish Rail
//
//  Created by Khatib Designs on 2/4/21.
//

import Foundation
import Alamofire
import SWXMLHash
import SwiftyXML

class API {
    
    var BASE_URL = "http://api.irishrail.ie/realtime/realtime.asmx/"
    
    //MARK: Get All Stations
    func getAllStationsXML(completion: @escaping (([Stations]) -> Void), failure: @escaping () -> Void) {
                       
        AF.request("\(BASE_URL)getAllStationsXML", method: .get, parameters: nil, encoding: URLEncoding.default, headers: .default).response { response in
            let status = response.response?.statusCode ?? 0
            var stations = [Stations]()
            if status == 200 {
                let xml = SWXMLHash.lazy(response.data!)
                do {
                    let ArrayOfObjStation: [Stations] = try xml["ArrayOfObjStation"]["objStation"].value()
                    for x in ArrayOfObjStation {
                        let station = Stations(StationDesc: x.StationDesc,
                                               StationLatitude: x.StationLatitude,
                                               StationLongitude: x.StationLongitude,
                                               StationCode: x.StationCode,
                                               StationId: x.StationId)
                        stations.append(station)

                    }
                    completion(stations)
                }catch {
                    print("Error")
                }
                return
            }
            
            failure()
        }
    }
    
    //MARK: Get All Stations By Type
    func getAllStationsXML(byType type: String, completion: @escaping (([Stations]) -> Void), failure: @escaping () -> Void) {
                       
        AF.request("\(BASE_URL)getAllStationsXML_WithStationType?StationType=\(type)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: .default).response { response in
            let status = response.response?.statusCode ?? 0
            var stations = [Stations]()
            if status == 200 {
                let xml = SWXMLHash.lazy(response.data!)
                do {
                    let ArrayOfObjStation: [Stations] = try xml["ArrayOfObjStation"]["objStation"].value()
                    for x in ArrayOfObjStation {
                        let station = Stations(StationDesc: x.StationDesc,
                                               StationLatitude: x.StationLatitude,
                                               StationLongitude: x.StationLongitude,
                                               StationCode: x.StationCode,
                                               StationId: x.StationId)
                        stations.append(station)

                    }
                    completion(stations)
                }catch {
                    print("Error")
                }
                return
            }
            
            failure()
        }
    }
    
    //MARK: Get Current Trains
    func getCurrentTrainsXML(completion: @escaping (([Trains]) -> Void), failure: @escaping () -> Void) {
                       
        AF.request("\(BASE_URL)getCurrentTrainsXML", method: .get, parameters: nil, encoding: URLEncoding.default, headers: .default).response { response in
            let status = response.response?.statusCode ?? 0
            var trains = [Trains]()
            if status == 200 {
                let xml = SWXMLHash.lazy(response.data!)
                do {
                    let ArrayOfObjTrainPositions: [Trains] = try xml["ArrayOfObjTrainPositions"]["objTrainPositions"].value()
                    for x in ArrayOfObjTrainPositions {
                        let train = Trains(TrainStatus: x.TrainStatus,
                                           TrainLatitude: x.TrainLatitude,
                                           TrainLongitude: x.TrainLongitude,
                                           TrainCode: x.TrainCode,
                                           TrainDate: x.TrainDate,
                                           PublicMessage: x.PublicMessage,
                                           Direction: x.Direction)
                        
                        trains.append(train)

                    }
                    completion(trains)
                }catch {
                    print("Error")
                }
                return
            }
            
            failure()
        }
    }
    
    //MARK: Get Current Trains By Type
    func getCurrentTrainsXML(byType type: String, completion: @escaping (([Trains]) -> Void), failure: @escaping () -> Void) {
                       
        AF.request("\(BASE_URL)getCurrentTrainsXML_WithTrainType?TrainType=\(type)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: .default).response { response in
            let status = response.response?.statusCode ?? 0
            var trains = [Trains]()
            if status == 200 {
                let xml = SWXMLHash.lazy(response.data!)
                do {
                    let ArrayOfObjTrainPositions: [Trains] = try xml["ArrayOfObjTrainPositions"]["objTrainPositions"].value()
                    for x in ArrayOfObjTrainPositions {
                        let train = Trains(TrainStatus: x.TrainStatus,
                                           TrainLatitude: x.TrainLatitude,
                                           TrainLongitude: x.TrainLongitude,
                                           TrainCode: x.TrainCode,
                                           TrainDate: x.TrainDate,
                                           PublicMessage: x.PublicMessage,
                                           Direction: x.Direction)
                        
                        trains.append(train)

                    }
                    completion(trains)
                }catch {
                    print("Error")
                }
                return
            }
            
            failure()
        }
    }
    
    //MARK: Get Station Data By Name
    func getStationData(byName name: String, completion: @escaping (([StationData]) -> Void), failure: @escaping () -> Void) {
                       
        AF.request("\(BASE_URL)getStationDataByNameXML?StationDesc=\(name)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: .default).response { response in
            let status = response.response?.statusCode ?? 0
            var stationsData = [StationData]()
            if status == 200 {
                let xml = SWXMLHash.lazy(response.data!)
                do {
                    let ArrayOfObjStationData: [StationData] = try xml["ArrayOfObjStationData"]["objStationData"].value()
                    for x in ArrayOfObjStationData {
                        let stationData = StationData(Servertime: x.Servertime,
                                                      Traincode: x.Traincode,
                                                      Stationfullname: x.Stationfullname,
                                                      Stationcode: x.Stationcode,
                                                      Querytime: x.Querytime,
                                                      Traindate: x.Traindate,
                                                      Origin: x.Origin,
                                                      Destination: x.Destination,
                                                      Origintime: x.Origintime,
                                                      Destinationtime: x.Destinationtime,
                                                      Status: x.Status,
                                                      Lastlocation: x.Lastlocation,
                                                      Duein: x.Duein,
                                                      Late: x.Late,
                                                      Exparrival: x.Exparrival,
                                                      Expdepart: x.Expdepart,
                                                      Scharrival: x.Scharrival,
                                                      Schdepart: x.Schdepart,
                                                      Direction: x.Direction,
                                                      Traintype: x.Traintype,
                                                      Locationtype: x.Locationtype)
                        
                        stationsData.append(stationData)

                    }
                    completion(stationsData)
                }catch {
                    print("Error")
                }
                return
            }
            
            failure()
        }
    }
    
    //MARK: Get Station Data By Name, Time
    func getStationData(byName name: String, time: String, completion: @escaping (([StationData]) -> Void), failure: @escaping () -> Void) {
        
        AF.request("\(BASE_URL)getStationDataByNameXML?StationDesc=\(name)&NumMins=\(time)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: .default).response { response in
            let status = response.response?.statusCode ?? 0
            var stationsData = [StationData]()
            
            if status == 200 {
                let xml = SWXMLHash.lazy(response.data!)
                do {
                    let ArrayOfObjStationData: [StationData] = try xml["ArrayOfObjStationData"]["objStationData"].value()
                    for x in ArrayOfObjStationData {
                        let stationData = StationData(Servertime: x.Servertime,
                                                      Traincode: x.Traincode,
                                                      Stationfullname: x.Stationfullname,
                                                      Stationcode: x.Stationcode,
                                                      Querytime: x.Querytime,
                                                      Traindate: x.Traindate,
                                                      Origin: x.Origin,
                                                      Destination: x.Destination,
                                                      Origintime: x.Origintime,
                                                      Destinationtime: x.Destinationtime,
                                                      Status: x.Status,
                                                      Lastlocation: x.Lastlocation,
                                                      Duein: x.Duein,
                                                      Late: x.Late,
                                                      Exparrival: x.Exparrival,
                                                      Expdepart: x.Expdepart,
                                                      Scharrival: x.Scharrival,
                                                      Schdepart: x.Schdepart,
                                                      Direction: x.Direction,
                                                      Traintype: x.Traintype,
                                                      Locationtype: x.Locationtype)
                        
                        stationsData.append(stationData)

                    }
                    completion(stationsData)
                }catch {
                    print("Error")
                }
                return
            }
            
            failure()
        }
    }
    
    //MARK: Get Station Data By Code
    func getStationData(byCode code: String, completion: @escaping (([StationData]) -> Void), failure: @escaping () -> Void) {
                       
        AF.request("\(BASE_URL)getStationDataByCodeXML?StationCode=\(code)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: .default).response { response in
            let status = response.response?.statusCode ?? 0
            var stationsData = [StationData]()
            
            if status == 200 {
                let xml = SWXMLHash.lazy(response.data!)
                do {
                    let ArrayOfObjStationData: [StationData] = try xml["ArrayOfObjStationData"]["objStationData"].value()
                    for x in ArrayOfObjStationData {
                        let stationData = StationData(Servertime: x.Servertime,
                                                      Traincode: x.Traincode,
                                                      Stationfullname: x.Stationfullname,
                                                      Stationcode: x.Stationcode,
                                                      Querytime: x.Querytime,
                                                      Traindate: x.Traindate,
                                                      Origin: x.Origin,
                                                      Destination: x.Destination,
                                                      Origintime: x.Origintime,
                                                      Destinationtime: x.Destinationtime,
                                                      Status: x.Status,
                                                      Lastlocation: x.Lastlocation,
                                                      Duein: x.Duein,
                                                      Late: x.Late,
                                                      Exparrival: x.Exparrival,
                                                      Expdepart: x.Expdepart,
                                                      Scharrival: x.Scharrival,
                                                      Schdepart: x.Schdepart,
                                                      Direction: x.Direction,
                                                      Traintype: x.Traintype,
                                                      Locationtype: x.Locationtype)
                        
                        stationsData.append(stationData)

                    }
                    completion(stationsData)
                }catch {
                    print("Error")
                }
                return
            }
            
            failure()
        }
    }
    
    //MARK: Get Station Data By Code, Time
    func getStationData(byCode code: String, time: String, completion: @escaping (([StationData]) -> Void), failure: @escaping () -> Void) {
        AF.request("\(BASE_URL)getStationDataByCodeXML_WithNumMins?StationCode=\(code)&NumMins=\(time)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: .default).response { response in
            let status = response.response?.statusCode ?? 0
            var stationsData = [StationData]()
            if status == 200 {
                let xml = SWXMLHash.lazy(response.data!)
                do {
                    let ArrayOfObjStationData: [StationData] = try xml["ArrayOfObjStationData"]["objStationData"].value()
                    for x in ArrayOfObjStationData {
                        let stationData = StationData(Servertime: x.Servertime,
                                                      Traincode: x.Traincode,
                                                      Stationfullname: x.Stationfullname,
                                                      Stationcode: x.Stationcode,
                                                      Querytime: x.Querytime,
                                                      Traindate: x.Traindate,
                                                      Origin: x.Origin,
                                                      Destination: x.Destination,
                                                      Origintime: x.Origintime,
                                                      Destinationtime: x.Destinationtime,
                                                      Status: x.Status,
                                                      Lastlocation: x.Lastlocation,
                                                      Duein: x.Duein,
                                                      Late: x.Late,
                                                      Exparrival: x.Exparrival,
                                                      Expdepart: x.Expdepart,
                                                      Scharrival: x.Scharrival,
                                                      Schdepart: x.Schdepart,
                                                      Direction: x.Direction,
                                                      Traintype: x.Traintype,
                                                      Locationtype: x.Locationtype)
                        
                        stationsData.append(stationData)

                    }
                    completion(stationsData)
                }catch {
                    print("Error")
                }
                return
            }
            
            failure()
        }
    }
    
    //MARK: Get Train Movements
    func getTrainMovements(TrainId: String, TrainDate: String, completion: @escaping (([TrainMovements]) -> Void), failure: @escaping () -> Void) {
                       
        AF.request("\(BASE_URL)getTrainMovementsXML?TrainId=\(TrainId.replacingOccurrences(of: " ", with: ""))&TrainDate=\(TrainDate.replacingOccurrences(of: " ", with: "%20"))", method: .get, parameters: nil, encoding: URLEncoding.default, headers: .default).response { response in
            let status = response.response?.statusCode ?? 0
            var movementsData = [TrainMovements]()
            
            if status == 200 {
                let xml = SWXMLHash.lazy(response.data!)
                do {
                    let ArrayOfObjTrainMovements: [TrainMovements] = try xml["ArrayOfObjTrainMovements"]["objTrainMovements"].value()
                    for x in ArrayOfObjTrainMovements {
                        let movementData = TrainMovements(Traincode: x.Traincode,
                                                         Traindate: x.Traindate,
                                                         LocationCode: x.LocationCode,
                                                         LocationFullName: x.LocationFullName,
                                                         LocationOrder: x.LocationOrder,
                                                         Locationtype: x.Locationtype,
                                                         TrainOrigin: x.TrainOrigin,
                                                         TrainDestination: x.TrainDestination,
                                                         ScheduledArrival: x.ScheduledArrival,
                                                         ScheduledDeparture: x.ScheduledDeparture,
                                                         ExpectedArrival: x.ExpectedArrival,
                                                         ExpectedDeparture: x.ExpectedDeparture,
                                                         Arrival: x.Arrival,
                                                         Departure: x.Departure,
                                                         AutoArrival: x.AutoArrival,
                                                         AutoDepart: x.AutoDepart,
                                                         StopType: x.StopType)
                        
                        movementsData.append(movementData)

                    }
                    completion(movementsData)
                }catch {
                    print("Error")
                }
                return
            }
            
            failure()
        }
    }
}
